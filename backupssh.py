import os
import subprocess
import datetime
from stat import S_ISREG, ST_CTIME, ST_MODE
import os, sys, time
import glob

TARGET_DIR = '/Users/rorr/.ssh'
BACKUP_DIR = '/Users/rorr/Google Drive/ssh-backups'


def get_command(command):
    sp = subprocess.Popen(command, stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          shell=True)
    out, err = sp.communicate()
    result = dict(errorcode=sp.returncode, stdout=out, stderr=err)


def get_sorted_files(target_dir, glob_pat):
    files = glob.glob(os.path.join(target_dir,glob_pat))
    files.sort(key=lambda x: os.path.getmtime(x))
    return files


def get_latest_archive_file(target_dir):
    backups =  get_sorted_files(target_dir, "*.zip")
    if backups:
        return backups[-1]
    else:
        return None


def main():
    stamp = datetime.datetime.now()
    datestamp = stamp.strftime("%Y_%m_%d_%H_%M_%S")
    latest_archive = get_latest_archive_file(BACKUP_DIR)
    if latest_archive:
        ctime = os.path.getmtime(latest_archive)
        files = get_sorted_files(TARGET_DIR, "*")
        # are there any files in this target that are newer than the archives?
        if files:
            new_files = [x for x in files if os.path.getmtime(x) > ctime]
            if new_files:
                gen_archive(datestamp)
    else:
        gen_archive(datestamp)
    pass


def gen_archive(datestamp):
    cmd = 'zip -v -r --encrypt "{backup_dir}/ssh_{stamp}.zip" {target}/*'.format(target=TARGET_DIR,
                                                                                 stamp=datestamp,
                                                                                 backup_dir=BACKUP_DIR)
    os.system(cmd)


# ####################MAIN ENTRY POINT AS A SCRIPT ################
if __name__ == '__main__':
    main()
