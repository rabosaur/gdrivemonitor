import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import PatternMatchingEventHandler

path = '/Users/rorr/Google Drive/TheSocieteMixo'

class MyHandler(PatternMatchingEventHandler):
    patterns=["*"]

    def process(self, event):
        """
        event.event_type
            'modified' | 'created' | 'moved' | 'deleted'
        event.is_directory
            True | False
        event.src_path
            path/to/observed/file
        """
        if 'created' == event.event_type:
            item = (event.event_type, event.is_directory, event.src_path, event )
        elif 'modified' == event.event_type:
            item = (event.event_type, event.is_directory, event.src_path, event)
        elif 'deleted' == event.event_type:
            item = (event.event_type, event.is_directory, event.src_path, event)
        elif 'moved' == event.event_type:
            item = (event.event_type, event.is_directory, event.src_path, event )
        print item

    def on_modified(self, event):
        self.process(event)

    def on_created(self, event):
        self.process(event)

    def on_deleted(self, event):
        self.process(event)

    def on_moved(self, event):
        self.process(event)


if __name__ == "__main__":
    observer = Observer()
    observer.schedule(MyHandler(), path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()