import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import PatternMatchingEventHandler
import os
import subprocess
import datetime
from stat import S_ISREG, ST_CTIME, ST_MODE
import os, sys, time
import glob


TARGET_DIR = '/Users/rorr/.ssh'
BACKUP_DIR = '/Users/rorr/Google Drive/ssh-backups'

class MyHandler(PatternMatchingEventHandler):
    patterns=["*"]

    def on_modified(self, event):
        self.process(event)

    def on_created(self, event):
        self.process(event)

    def on_deleted(self, event):
        self.process(event)

    def on_moved(self, event):
        self.process(event)

    def process(self, event):
        """
        event.event_type
            'modified' | 'created' | 'moved' | 'deleted'
        event.is_directory
            True | False
        event.src_path
            path/to/observed/file
        """
        if 'created' == event.event_type:
            item = (event.event_type, event.is_directory, event.src_path, event )
        elif 'modified' == event.event_type:
            item = (event.event_type, event.is_directory, event.src_path, event)
        elif 'deleted' == event.event_type:
            item = (event.event_type, event.is_directory, event.src_path, event)
        elif 'moved' == event.event_type:
            item = (event.event_type, event.is_directory, event.src_path, event )
        print item
        # any event at all causes the backup to be run
        do_backup_to_archive()



def get_command(command):
    sp = subprocess.Popen(command, stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          shell=True)
    out, err = sp.communicate()
    result = dict(errorcode=sp.returncode, stdout=out, stderr=err)


def get_sorted_files(target_dir, glob_pat):
    files = glob.glob(os.path.join(target_dir,glob_pat))
    files.sort(key=lambda x: os.path.getmtime(x))
    return files


def get_latest_archive_file(target_dir):
    backups =  get_sorted_files(target_dir, "*.zip")
    if backups:
        return backups[-1]
    else:
        return None


def do_backup_to_archive():
    stamp = datetime.datetime.now()
    datestamp = stamp.strftime("%Y_%m_%d_%H_%M_%S")
    latest_archive = get_latest_archive_file(BACKUP_DIR)
    if latest_archive:
        ctime = os.path.getmtime(latest_archive)
        files = get_sorted_files(TARGET_DIR, "*")
        # are there any files in this target that are newer than the archives?
        if files:
            new_files = [x for x in files if os.path.getmtime(x) > ctime]
            if new_files:
                gen_archive(datestamp)
    else:
        gen_archive(datestamp)
    pass


def gen_archive(datestamp):
    print "Change detected, making archive"
    cmd = 'zip -v -r --encrypt -P password "{backup_dir}/ssh_{stamp}.zip" {target}/*'.format(target=TARGET_DIR,
                                                                                 stamp=datestamp,
                                                                                 backup_dir=BACKUP_DIR)
    os.system(cmd)




if __name__ == "__main__":
    observer = Observer()
    observer.schedule(MyHandler(), TARGET_DIR, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()